Fei-d
=====

Is a small js library to fade in and out html elements with css3(opacity/transitions).

HOW TO USE
-----------------------

Write it in the head of your html code: 

	<script type="text/javascript" src="fei-d.js'>
		//basic syntax
		feid(element, duration);
	</script>
	

Now you just need to assign it in a variable, like this:

	var myfade = feid(mydiv, 2);

To finish call te method on() or off() to fade-in or fade-out the element:

	myfade.on(); or myfade.off();

SUPPORT
--------
* IE10 - works fine
* CHROME - works very very well
* FIREFOX 17/18 - works fine
* OPERA 11 - works fine
* SAFARI - works fine too

TODO
-----
* Add a callback
* Fade-in elements with display:'none' active.

WANT TO CONTRIBUTE
------------
All help is needed, so read the license to do what the fuck you want to do starting to fork this repository.


LICENSE
--------
	DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
	Version 2, December 2004

	Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

	Everyone is permitted to copy and distribute verbatim or modified
	copies of this license document, and changing it is allowed as long
	as the name is changed.

	DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  	TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 	0. You just DO WHAT THE FUCK YOU WANT TO.

