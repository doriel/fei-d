Fei-d
=====

É uma pequena biblioteca em javascript que permite fazer o fade in e out de elementos html com css3(opacity/transitions).

COMO USAR
-----------------------

Adicione o script fei-d.js no head do teu código html: 

	<script type="text/javascript" src="fei-d.js'>
		//basic syntax
		feid(element, duration);
	</script>
	

Agora é só atribuir isto numa variavel que ficará assim:

	var myfade = feid(mydiv, 2);

pra finalizar é só chamar um dos dois metódos on() e o off() para fede in ou out do elemento.

	myfade.on(); or myfade.off();

SUPORTE
--------
* IE10 - funciona bem
* CHROME - funciona muito^2 bem
* FIREFOX 17/18 - funciona bem
* OPERA 11 - funciona bem
* SAFARI - funciona bem tambem

TODO
-----
* Adicionar um callback function
* Fade-in de elementos com a propriedade display declarada deste jeito:

	display:'none' active.

QUER CONTRIBUIR
------------
Toda a ajuda é necessária, por isso leia a licença para fazer o que diabos você quer fazer, começando pelo fork deste repositório.


LICENÇA
--------
	DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
	Version 2, December 2004

	Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

	Everyone is permitted to copy and distribute verbatim or modified
	copies of this license document, and changing it is allowed as long
	as the name is changed.

	DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  	TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 	0. You just DO WHAT THE FUCK YOU WANT TO.

